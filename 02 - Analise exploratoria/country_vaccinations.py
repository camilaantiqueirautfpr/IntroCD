import pandas as pd
import matplotlib.pyplot as plt
import seaborn as sns

# Ler o dataset
df_vaccination_progress = pd.read_csv('country_vaccinations.csv')

# Mostrar as primeiras linhas do dataset
df_vaccination_progress.head()

# Verificação de valores faltantes em diferentes colunas
missing_values_progress = df_vaccination_progress.isnull().sum()
missing_values_progress

# Preenchimento dos valores faltantes com zero
df_vaccination_progress.fillna(0, inplace=True)

# Verificação dos valores faltantes para confirmar que foram preenchidos
missing_values_progress_after_fill = df_vaccination_progress.isnull().sum()
missing_values_progress_after_fill

# Gráficos 
# Variáveis numéricas de interesse para a análise de distribuição
progress_vars = ['total_vaccinations', 'people_vaccinated', 'people_fully_vaccinated', 
                 'daily_vaccinations', 'total_vaccinations_per_hundred', 
                 'people_vaccinated_per_hundred', 'people_fully_vaccinated_per_hundred']

# Histogramas para as variáveis selecionadas
fig, axes = plt.subplots(len(progress_vars), 1, figsize=(12, 20))
fig.suptitle('Distribuição dos Dados de Progresso de Vacinação')

for i, var in enumerate(progress_vars):
    sns.histplot(df_vaccination_progress[var], kde=False, bins=30, ax=axes[i])
    axes[i].set_title(f'Distribuição de {var.replace("_", " ").title()}')
    axes[i].set_xlabel(var)
    axes[i].set_ylabel('Frequência')

plt.tight_layout(rect=[0, 0, 1, 0.98])
plt.show()

# Identificação de Padrões Iniciais
# Matriz de correlação para as variáveis selecionadas
correlation_matrix_progress = df_vaccination_progress[progress_vars].corr()

# Heatmap para visualizar as correlações
plt.figure(figsize=(12, 8))
sns.heatmap(correlation_matrix_progress, annot=True, cmap='coolwarm', fmt='.2f')
plt.title('Matriz de Correlação para Dados de Progresso de Vacinação')
plt.show()

correlation_matrix_progress

# Explporações iniciais
# Explorando quais países têm as taxas mais altas de vacinação por 100 habitantes
top_countries_vaccination_rate = df_vaccination_progress.groupby('country')['total_vaccinations_per_hundred'].max().sort_values(ascending=False).head(10)

# Gráfico de barras
plt.figure(figsize=(12, 8))
top_countries_vaccination_rate.sort_values().plot(kind='barh', color='skyblue')
plt.title('Top 10 Países com as Maiores Taxas de Vacinação por 100 Habitantes')
plt.xlabel('Total de Vacinações por 100 Habitantes')
plt.ylabel('Países')
plt.show()

# Explorando quais vacinas são mais comumente usadas em diferentes países
common_vaccines = df_vaccination_progress.groupby('vaccines')['country'].nunique().sort_values(ascending=False).head(10)

# Gráfico de barras
plt.figure(figsize=(12, 8))
common_vaccines.sort_values().plot(kind='barh', color='green')
plt.title('Top 10 Vacinas Mais Comuns por Número de Países Usando')
plt.xlabel('Número de Países')
plt.ylabel('Vacinas')
plt.show()

# Pegando o valor mais recente do total de vacinações de cada país
latest_vaccination_data_updated = df_vaccination_progress.groupby("country").last().reset_index()

# Selecionando os 10 países com o maior valor do total mais recente de vacinações
top_10_countries_vaccinations_updated = latest_vaccination_data_updated.nlargest(10, 'total_vaccinations')

# Plotando o gráfico
plt.figure(figsize=(14, 8))
sns.barplot(x=top_10_countries_vaccinations_updated['country'], y=top_10_countries_vaccinations_updated['total_vaccinations'], palette="viridis", log=True)
plt.title('Top 10 Países com Maior Valor do Total Mais Recente de Vacinações (Escala Logarítmica)')
plt.ylabel('Total de Vacinações (Log)')
plt.xlabel('País')
plt.xticks(rotation=45)
plt.show()

# Gráfico com escala logarítmica para o total de vacinações nos 10 países que mais vacinaram
plt.figure(figsize=(14, 8))
sns.barplot(x=top_10_countries_vaccinations_updated['country'], y=top_10_countries_vaccinations_updated['total_vaccinations'], palette="viridis", log=True)
plt.title('Total Mais Recente de Vacinações (Top 10 Países) - Escala Logarítmica')
plt.ylabel('Total de Vacinações (Log)')
plt.xlabel('País')
plt.xticks(rotation=45)
plt.show()

# Gráfico com escala logarítmica para a quantidade de pessoas totalmente vacinadas no 10 países que mais vacinaram
plt.figure(figsize=(14, 8))
sns.barplot(x=top_10_countries_vaccinations_updated['country'], y=top_10_countries_vaccinations_updated['people_fully_vaccinated'], palette="viridis", log=True)
plt.title('Total Mais Recente de Pessoas Totalmente Vacinadas (Top 10 Países) - Escala Logarítmica')
plt.ylabel('Pessoas Totalmente Vacinadas (Log)')
plt.xlabel('País')
plt.xticks(rotation=45)
plt.show()

# Calculando a proporção entre os valores de pessoas totalmente vacinadas e o total de vacinações para os 10 países
proportion_fully_vaccinated_vs_total = (top_10_countries_vaccinations_updated['people_fully_vaccinated'] / 
                                        top_10_countries_vaccinations_updated['total_vaccinations'])

# Plotando o gráfico
plt.figure(figsize=(14, 8))
sns.barplot(x=top_10_countries_vaccinations_updated['country'], y=proportion_fully_vaccinated_vs_total, palette="viridis")
plt.title('Proporção de Pessoas Totalmente Vacinadas em relação ao Total de Vacinações (Top 10 Países)')
plt.ylabel('Proporção')
plt.xlabel('País')
plt.xticks(rotation=45)
plt.show()
