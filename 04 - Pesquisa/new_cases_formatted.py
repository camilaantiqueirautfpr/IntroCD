import pandas as pd
import matplotlib.pyplot as plt


countries_df = pd.read_csv('coutries.csv')
clusters_df = pd.read_csv('countries_clusters.csv')
merged_df = pd.merge(countries_df, clusters_df, on='Country')
population_per_cluster = merged_df.groupby('Cluster')['Population'].sum()
grouped_df = merged_df.groupby(['Cluster', 'Week'])['New_cases'].sum().reset_index()
grouped_df['New_cases_normalized'] = grouped_df.apply(
    lambda row: row['New_cases'] / population_per_cluster[row['Cluster']], axis=1
)
cluster_color_map = {1: '#1f77b4', 2: '#ff7f0e', 3: '#2ca02c', 4: '#d62728'}
plt.figure(figsize=(12, 6))
for cluster in grouped_df['Cluster'].unique():
    cluster_data = grouped_df[grouped_df['Cluster'] == cluster]
    plt.plot(cluster_data['Week'], cluster_data['New_cases_normalized'], label=f'Cluster {cluster}', color=cluster_color_map[cluster])

plt.title('Novos registros de mortes por Semana - Todos os Clusters')
plt.xlabel('Semana')
plt.ylabel('Novos Casos (por População)')
plt.legend()
plt.show()
