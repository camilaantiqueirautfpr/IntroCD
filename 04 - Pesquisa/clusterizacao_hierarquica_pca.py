from sklearn.preprocessing import MinMaxScaler
from sklearn.decomposition import PCA
from scipy.cluster.hierarchy import dendrogram, linkage, fcluster
import matplotlib.pyplot as plt
import pandas as pd

# Carregar o dataset
file_path = 'countries_dataset.csv'  # Substitua pelo caminho do seu arquivo
data = pd.read_csv(file_path)

# Selecionar as variáveis de interesse incluindo a coluna 'Country'
variables = ['Country', 'Human Development Index (2021)', 'percentage_vaccinated', 'GDP', 'Life expectancy', 'Physicians per thousand', 'Unemployment rate']
data_cluster = data[variables]

# Remover registros com valores faltantes
data_cluster = data_cluster.dropna()

# Separar a coluna 'Country'
countries = data_cluster['Country']
data_cluster = data_cluster.drop(columns=['Country'])

# Normalizar os dados
scaler = MinMaxScaler()
for var in data_cluster.columns:
    data_cluster[var] = scaler.fit_transform(data_cluster[[var]])

# Realizar a clusterização hierárquica
linked = linkage(data_cluster, method='ward')

# Criar o dendrograma
plt.figure(figsize=(12, 7))
dendrogram(linked, orientation='top', distance_sort='descending', show_leaf_counts=True)
plt.title('Dendrograma de Clusterização Hierárquica')
plt.xlabel('Índice do País')
plt.ylabel('Distância')
plt.show()

# Determinar a quantidade de clusters
max_d = 2  # Este valor pode ser ajustado conforme a necessidade
clusters = fcluster(linked, max_d, criterion='distance')

# Aplicar PCA para reduzir para 3 componentes
pca = PCA(n_components=3)
principal_components = pca.fit_transform(data_cluster)

# Imprimir as variáveis mais importantes para cada componente principal
print("Componentes da PCA:")
for i, component in enumerate(pca.components_, start=1):
    print(f"\nComponente Principal {i}:")
    print("Variação Explicada:", pca.explained_variance_ratio_[i-1])
    for val, var in sorted(zip(component, data_cluster.columns), reverse=True):
        print(f"{var}: {val}")

# Criar um DataFrame com os componentes principais
pca_df = pd.DataFrame(data=principal_components, columns=['PC1', 'PC2', 'PC3'])

# Adicionar a coluna 'Country' e os rótulos de cluster ao DataFrame do PCA
pca_df = pd.concat([countries.reset_index(drop=True), pca_df], axis=1)
pca_df['Cluster'] = clusters

# Plotar em 3D com legenda
fig = plt.figure(figsize=(10, 7))
ax = fig.add_subplot(111, projection='3d')

# Colorir os pontos com base nos clusters e adicionar rótulos para a legenda
for cluster in set(clusters):
    cluster_points = pca_df[pca_df['Cluster'] == cluster]
    ax.scatter(cluster_points['PC1'], cluster_points['PC2'], cluster_points['PC3'],
               label=f'Cluster {cluster}')

# Adicionar rótulos aos eixos e legenda
ax.set_xlabel('PC1')
ax.set_ylabel('PC2')
ax.set_zlabel('PC3')
ax.legend()

# Título e mostrar o gráfico
plt.title('Visualização 3D PCA com Clusters')
plt.show()

# Adicionar rótulos de cluster ao dataset original e agrupar os países
data['Cluster'] = clusters
clustered_countries = data.groupby('Cluster')['Country'].apply(list)

# Converter o agrupamento em um DataFrame
clustered_countries_df = pd.DataFrame(clustered_countries).reset_index()

# Renomear as colunas
clustered_countries_df.columns = ['Cluster', 'Countries']


# Salvar o resultado em um arquivo CSV
clustered_countries_df.to_csv('clustered_countries.csv', index=False)

pca_df.to_csv('countries_clusters.csv', index=False)

# Exibir o DataFrame final
clustered_countries_df
