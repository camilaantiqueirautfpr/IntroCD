countries_dataset = pd.read_csv('countries_dataset.csv')
who_covid19_dataset = pd.read_csv('WHO-COVID-19-global-data.csv')

# Preparar o dataset de COVID-19
who_covid19_dataset['Date_reported'] = pd.to_datetime(who_covid19_dataset['Date_reported'])
who_covid19_dataset['Week'] = who_covid19_dataset['Date_reported'].dt.isocalendar().week
covid_weekly = who_covid19_dataset.groupby(['Country', 'Week']).agg({'New_cases': 'sum', 'New_deaths': 'sum'}).reset_index()

# Normalizar os dados de COVID-19 pela população
covid_weekly = covid_weekly.merge(countries_data[['Country', 'Population']], on='Country')
covid_weekly['Cases_per_Capita'] = covid_weekly['New_cases'] / covid_weekly['Population']
covid_weekly['Deaths_per_Capita'] = covid_weekly['New_deaths'] / covid_weekly['Population']

# Identify countries present in both datasets
common_countries = set(countries_dataset['Country']).intersection(set(covid_weekly['Country']))

# Identify countries present in only one dataset
countries_only_in_first = set(countries_dataset['Country']) - common_countries
countries_only_in_second = set(covid_weekly['Country']) - common_countries

# Merge datasets based on common countries
merged_dataset = pd.merge(countries_dataset[countries_dataset['Country'].isin(common_countries)],
                          who_covid19_dataset[who_covid19_dataset['Country'].isin(common_countries)],
                          on='Country')

# Display the first few rows of the merged dataset and the countries unique to each dataset
merged_dataset.head(), countries_only_in_first, countries_only_in_second


# Adjusting country names for the United States and the United Kingdom in both datasets
countries_dataset.replace({'United States': 'United States of America', 'United Kingdom': 'The United Kingdom'}, inplace=True)
who_covid19_dataset.replace({'United States of America': 'United States of America', 'The United Kingdom': 'The United Kingdom'}, inplace=True)

# Recompute the common countries after the name adjustments
common_countries_updated = set(countries_dataset['Country']).intersection(set(who_covid19_dataset['Country']))

# Merge datasets based on the updated common countries list
merged_dataset_updated = pd.merge(countries_dataset[countries_dataset['Country'].isin(common_countries_updated)],
                                  who_covid19_dataset[who_covid19_dataset['Country'].isin(common_countries_updated)],
                                  on='Country')

# Display the first few rows of the updated merged dataset
merged_dataset_updated.head()
