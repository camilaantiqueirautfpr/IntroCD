import pandas as pd
import matplotlib.pyplot as plt

# Carregar os datasets
countries_df = pd.read_csv('coutries.csv')
clusters_df = pd.read_csv('countries_clusters.csv')

# Unir os datasets
merged_df = pd.merge(countries_df, clusters_df, on='Country')

# Calcular a população total de cada cluster
population_per_cluster = merged_df.groupby('Cluster')['Population'].sum()

# Agrupar por 'Cluster' e 'Week', e somar 'New_cases'
grouped_df = merged_df.groupby(['Cluster', 'Week'])['New_cases'].sum().reset_index()

# Normalizar os novos casos pela população do cluster
grouped_df['New_cases_normalized'] = grouped_df.apply(
    lambda row: row['New_cases'] / population_per_cluster[row['Cluster']], axis=1
)

# Plotar todas as linhas dos clusters no mesmo gráfico
plt.figure(figsize=(12, 6))
for cluster in grouped_df['Cluster'].unique():
    cluster_data = grouped_df[grouped_df['Cluster'] == cluster]
    plt.plot(cluster_data['Week'], cluster_data['New_cases_normalized'], label=f'Cluster {cluster}')

plt.title('Novos Casos Normalizados por Semana - Todos os Clusters')
plt.xlabel('Semana')
plt.ylabel('Novos Casos (por População)')
plt.legend()
plt.show()
